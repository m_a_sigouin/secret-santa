from django.shortcuts import get_object_or_404, get_list_or_404, render, redirect
from django.http import HttpResponse, JsonResponse

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from django.core.validators import validate_email
from django.core.exceptions import ValidationError

from django.views.decorators.csrf import csrf_exempt

from django.db import IntegrityError
from django.db.models import Q

from django.utils import timezone

from .models import Exchange, Participant

import random

import json

# Create your views here.


def index(request):
    return render(request, "secret_santa/index.html")


def login_view(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        
        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            
            return redirect("index")
        else:
            return render(request, "secret_santa/login.html", {
                "message": "Invalid username and/or password."
            })
    else:
        return render(request, "secret_santa/login.html")


@login_required(login_url="/login")
def logout_view(request):
    logout(request)

    return redirect("index")


def register(request):
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST["email"]
        first_name = request.POST["first-name"]
        last_name = request.POST["last-name"]
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        
        if (
            username is None
            or email is None
            or first_name is None
            or last_name is None
            or password is None
            or confirmation is None
        ):
            return render_register_page_with_error(
                request, "Please fill all the required fields.", username,
                email, first_name, last_name,
                password, confirmation
            )
        elif password != confirmation:
            return render_register_page_with_error(
                request, "Password and confirmation don't match.", username,
                email, first_name, last_name,
                password, confirmation
            )
        elif not check_email(email):
            return render_register_page_with_error(
                request, "Email is invalid.", username,
                email, first_name, last_name,
                password, confirmation
            )
        else:
            if User.objects.filter(email=email).exists():
                return render_register_page_with_error(
                    request, "Email address already used.", username,
                    email, first_name, last_name,
                    password, confirmation
                )

            try:
                user = User.objects.create_user(username=username, email=email, password=password)
                
                user.first_name = first_name
                user.last_name = last_name
                
                user.save()
            except IntegrityError:
                return render_register_page_with_error(
                    request, "Username already used.", username,
                    email, first_name, last_name,
                    password, confirmation
                )
            
            login(request, user)

            exchanges_with_participants_using_same_email = Exchange.objects.filter(
                exchange_date__gte=timezone.now().date(),
                participants__email=email
            )

            for exchange in exchanges_with_participants_using_same_email:
                participant = exchange.participants.get(email=email)

                participant.linked_user = user

                participant.save()
            
            return redirect("index")
    else:
        return render(request, "secret_santa/register.html")


@login_required(login_url="/login")
def create_exchange(request):
    if request.method == "POST":
        request_dict = request.POST.dict()

        for key in request_dict:
            if "csrf" in key:
                request_dict.pop(key)
                break

        name = request_dict.pop("name")
        description = request_dict.pop("description")
        budget = request_dict.pop("budget")
        date = request_dict.pop("date")

        participants = []
        
        for key in request_dict:
            if key.startswith("firstname"):
                key_digit = key.strip("firstname")

                participant = {
                    "firstname": request_dict[key],
                    "lastname": request_dict[f"lastname{key_digit}"],
                    "email": request_dict[f"email{key_digit}"]
                }

                for existing_participant in participants:
                    if existing_participant["email"] == participant["email"]:
                        return render_create_exchange_page_with_error(
                            request, "Two participants cannot share the same email address.",
                            name, description, budget, date
                        )

                participants.append(participant)
        
        if len(participants) < 3:
            return render_create_exchange_page_with_error(
                request, "Exchange must have at least three participants.",
                name, description, budget, date
            )

        newExchange = Exchange(
            group_name=name,
            description=description,
            budget=budget,
            created=timezone.now(),
            exchange_date=date,
            organizer=request.user
            )
        
        newExchange.save()

        for participant in participants:
            new_participant = Participant(
                first_name=participant["firstname"],
                last_name=participant["lastname"],
                email=participant["email"]
            )

            if User.objects.filter(email=participant["email"]).exists():
                new_participant.linked_user = User.objects.get(email=participant["email"])
            
            new_participant.save()

            newExchange.participants.add(new_participant)
        
        newExchange.save()

        return redirect("exchanges")
    else:
        return render(request, "secret_santa/create.html")


@login_required(login_url="/login")
def edit_exchange(request, exchange_id):
    if not Exchange.objects.filter(
        Q(organizer=request.user) | Q(participants__linked_user=request.user),
        pk=exchange_id
    ).exists():
        return JsonResponse({"error": "Invalid request."}, status=400)
    
    exchange = Exchange.objects.get(pk=exchange_id)

    if request.method == "POST":
        data = json.loads(request.body)

        if exchange.organizer == request.user:
            group_name = data.get("group_name", "")
            description = data.get("description", "")
            budget = data.get("budget", "")
            exchange_date = data.get("date", "")

            if (
                exchange.group_name == group_name
                and exchange.description == description
                and exchange.budget == budget
                and exchange.exchange_date == exchange_date
            ):
                return JsonResponse({"error": "Nothing changed."}, status=400)

            if group_name == "" or budget == "" or exchange_date == "": # decription CAN be an empty string.
                return JsonResponse({"error": "Invalid request."}, status=400)
            
            exchange.group_name = group_name
            exchange.description = description
            exchange.budget = budget
            exchange.exchange_date = exchange_date

            exchange.save()

            return JsonResponse({"message": "Modified exchange information."}, status=201)
        else:
            return JsonResponse({"error": "Unauthorized action."}, status=400)
    
    return JsonResponse({"error": "Invalid request."}, status=400)


@login_required(login_url="/login")
def add_participant_to_exchange(request, exchange_id):
    if not Exchange.objects.filter(
        Q(organizer=request.user) | Q(participants__linked_user=request.user),
        pk=exchange_id
    ).exists():
        return JsonResponse({"error": "Invalid request."}, status=400)
    
    exchange = Exchange.objects.get(pk=exchange_id)

    if request.method == "POST":
        data = json.loads(request.body)

        if exchange.organizer == request.user:
            first_name = data.get("firstname", "")
            last_name = data.get("lastname", "")
            email = data.get("email", "")

            if first_name == "" or last_name == "" or email == "":
                return JsonResponse({"error": "Invalid request."}, status=400)
            
            if not check_email(email):
                return JsonResponse({"error": "Email invalid"}, status=400)

            new_participant = Participant(
                first_name=first_name,
                last_name=last_name,
                email=email
            )

            if User.objects.filter(email=email).exists():
                new_participant.linked_user = User.objects.get(email=email)
            
            new_participant.save()

            exchange.participants.add(new_participant)

            exchange.save()

            return JsonResponse({"message": f"{first_name} {last_name} added to participants."}, status=201)
        else:
            return JsonResponse({"error": "Unauthorized action."}, status=400)
    
    return JsonResponse({"error": "Invalid request."}, status=400)


@login_required(login_url="/login")
def view_user_exchanges(request):
    ongoing_exchanges = Exchange.objects.filter(
        Q(organizer=request.user) | Q(participants__linked_user=request.user),
        exchange_date=timezone.now().date()
    ).distinct()
    ongoing_exchanges_created = Exchange.objects.filter(
        exchange_date=timezone.now().date(),
        organizer=request.user
    ).distinct()
    ongoing_exchanges_attending = Exchange.objects.filter(
        exchange_date=timezone.now().date(),
        participants__linked_user=request.user,
        participants__participation=Participant.CONFIRMED
    ).distinct()
    ongoing_exchanges_invited = Exchange.objects.filter(
        exchange_date=timezone.now().date(),
        participants__linked_user=request.user,
        participants__participation=Participant.INVITED
    ).distinct()
    
    future_exchanges = Exchange.objects.filter(
        Q(organizer=request.user) | Q(participants__linked_user=request.user),
        exchange_date__gt=timezone.now().date()
    ).distinct().order_by("exchange_date")
    future_exchanges_created = Exchange.objects.filter(
        exchange_date__gt=timezone.now().date(),
        organizer=request.user
    ).distinct().order_by("exchange_date")
    future_exchanges_attending = Exchange.objects.filter(
        exchange_date__gt=timezone.now().date(),
        participants__linked_user=request.user,
        participants__participation=Participant.CONFIRMED
    ).distinct().order_by("exchange_date")
    future_exchanges_invited = Exchange.objects.filter(
        exchange_date__gt=timezone.now().date(),
        participants__linked_user=request.user,
        participants__participation=Participant.INVITED
    ).distinct().order_by("exchange_date")

    past_exchanges_created = Exchange.objects.filter(
        exchange_date__lt=timezone.now().date(),
        organizer=request.user
    ).order_by("-exchange_date")

    return render(request, "secret_santa/exchange-browser.html", {
        "ongoing_exchanges": ongoing_exchanges,
        "future_exchanges": future_exchanges,
        "created_ongoing_exchanges": ongoing_exchanges_created,
        "created_future_exchanges": future_exchanges_created,
        "created_past_exchanges": past_exchanges_created,
        "attending_ongoing_exchanges": ongoing_exchanges_attending,
        "attending_future_exchanges": future_exchanges_attending,
        "invited_ongoing_exchanges": ongoing_exchanges_invited,
        "invited_future_exchanges": future_exchanges_invited
    })


@login_required(login_url="/login")
def view_exchange(request, exchange_id):
    if not Exchange.objects.filter(
        Q(organizer=request.user) | Q(participants__linked_user=request.user),
        pk=exchange_id
    ).exists():
        return redirect("index")
    
    exchange = Exchange.objects.get(pk=exchange_id)

    return render(request, "secret_santa/exchange.html", {
        "exchange": exchange,
        "is_past_exchange": exchange.exchange_date < timezone.now().date()
    })


@login_required(login_url="/login")
@csrf_exempt
def invite_reply(request, exchange_id):
    if request.method == "PUT":
        data = json.loads(request.body)

        if (
            data.get("answer") is not None
            and data.get("participant_id") is not None
        ):
            if (
                Exchange.objects.filter(pk=exchange_id).exists()
                and Participant.objects.filter(pk=data["participant_id"]).exists()
            ):
                participant = Participant.objects.get(pk=data["participant_id"])

                if participant.linked_user != request.user:
                    return JsonResponse({"error": "Unauthorized action."}, status=400)

                if participant in Exchange.objects.get(pk=exchange_id).participants.all():
                    if data["answer"] == "Y":
                        participant.participation = Participant.CONFIRMED
                    elif data["answer"] == "N":
                        participant.participation = Participant.DECLINED
                    else:
                        return JsonResponse({"error": "Invalid command"}, status=400)
                    
                    participant.save()

                    return HttpResponse(status=204)
                
                return JsonResponse({"error": "Participant not found."}, status=400)
        
    return JsonResponse({"error": "Invalid request"}, status=400)


@login_required(login_url="/login")
@csrf_exempt
def draw_names(request, exchange_id):
    if not Exchange.objects.filter(pk=exchange_id, organizer=request.user).exists():
        return JsonResponse({"error": "Invalid request"}, status=400)
    
    exchange = Exchange.objects.get(pk=exchange_id)

    if request.method == "POST":
        confirmed_participants = exchange.participants.filter(participation=Participant.CONFIRMED)
        other_participants = exchange.participants.exclude(participation=Participant.CONFIRMED)

        index_array = []

        for participant in confirmed_participants:
            index_array.append(participant.pk)
        
        while True:
            random.shuffle(index_array)

            is_shuffle_valid = True

            for i in range(confirmed_participants.count()):
                if confirmed_participants[i].pk == index_array[i]:
                    is_shuffle_valid = False
                    break
            
            if is_shuffle_valid:
                break
        
        for i in range(confirmed_participants.count()):
            confirmed_participants[i].picked_participant = Participant.objects.get(pk=index_array[i])
            confirmed_participants[i].save()
        
        for participant in other_participants:
            if participant.participation == Participant.INVITED:
                participant.participation = Participant.NOT_ANSWERED
                participant.save()
        
        exchange.draw_made = True
        exchange.save()
        
        return JsonResponse({"message": "Draw successful"}, status=201)
        
    return JsonResponse({"error": "Invalid request"}, status=400)


@login_required(login_url="/login")
@csrf_exempt
def reset_draw(request, exchange_id):
    if not Exchange.objects.filter(pk=exchange_id, organizer=request.user):
        return JsonResponse({"error": "Invalid request"}, status=400)
    
    if request.method == "PUT":
        exchange = Exchange.objects.get(pk=exchange_id)

        exchange.draw_made = False

        exchange.save()

        for participant in exchange.get_confirmed_participants():
            participant.picked_participant = None

            participant.save()
        
        for participant in exchange.get_not_participating():
            if participant.participation == Participant.NOT_ANSWERED:
                participant.participation = Participant.INVITED

                participant.save()
        
        return JsonResponse({"message": "Reset successful"}, status=200)
    
    return JsonResponse({"error": "Invalid request"}, status=400)


@login_required(login_url="/login")
def get_picked_name(request, exchange_id, participant_id):
    if request.method != "GET":
        return JsonResponse({"error": "Bad Request"}, status=400)

    if (
        Participant.objects.filter(pk=participant_id).exists()
        and Exchange.objects.filter(pk=exchange_id).exists()
    ):
        participant = Participant.objects.get(pk=participant_id)

        if participant not in Exchange.objects.get(pk=exchange_id).participants.all():
            return JsonResponse({"error": "Invalid Request"}, status=400)
        
        return JsonResponse({"name": f"{participant.picked_participant.first_name} {participant.picked_participant.last_name}"}, status=200)

    return JsonResponse({"error": "Bad Request"}, status=400)


@login_required(login_url="/login")
@csrf_exempt
def cancel_exchange(request, exchange_id):
    if not Exchange.objects.filter(pk=exchange_id, organizer=request.user).exists():
        return JsonResponse({"error": "Invalid request."}, status=400)
    
    if request.method != "PUT":
        return JsonResponse({"error": "Bad request."}, status=400)
    
    exchange = Exchange.objects.get(pk=exchange_id)

    data = json.loads(request.body)

    if data.get("cancel") is not None:
        exchange.cancelled = data["cancel"]

        exchange.save()

        return JsonResponse({"message": "Exchange " + "cancelled" if data["cancel"] == True else "reactivated"}, status=200)
    
    return JsonResponse({"error": "Invalid request."}, status=400)


def render_register_page_with_error(request, message, username, email, first_name, last_name, password, confirmation):
    return render(request, "secret_santa/register.html", {
        "message": message,
        "username": username,
        "email": email,
        "first_name": first_name,
        "last_name": last_name,
        "password": password,
        "confirmation": confirmation
    })


def render_create_exchange_page_with_error(request, message, name, description, budget, date):
    return render(request, "secret_santa/create.html", {
        "message": message,
        "name": name,
        "description": description,
        "budget": budget,
        "date": date
    })


def check_email(email):
    try:
        validate_email(email)

        return True
    except ValidationError:
        return False

