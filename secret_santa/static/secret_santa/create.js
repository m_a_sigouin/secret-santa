function addParticipant() {
    const participantsContainer = document.getElementById("participants");
    const participantNumber = participantsContainer.children.length;
    const newParticipantId = participantNumber + 1;

    const newParticipantForm = document.createElement("fieldset");
    newParticipantForm.setAttribute("id", `participant${newParticipantId}`);
    newParticipantForm.classList.add("border", "border-2", "rounded", "mb-3", "p-3")

    const formHeader = document.createElement("div");
    formHeader.classList.add("mb-3");

    const formTitle = document.createElement("h5");
    formTitle.classList.add("d-inline", "align-middle", "fs-3", "mb-0", "me-3");
    formTitle.innerHTML = `Participant ${newParticipantId}`;

    formHeader.appendChild(formTitle);

    const formDeleteParticipantButton = document.createElement("button");
    formDeleteParticipantButton.classList.add("d-inline", "delete", "btn", "btn-outline-danger", "fs-6");
    formDeleteParticipantButton.setAttribute("type", "button");
    formDeleteParticipantButton.onclick = () => {
        deleteParticipant(`participant${newParticipantId}`);
    };
    formDeleteParticipantButton.innerHTML = "Remove";

    formHeader.appendChild(formDeleteParticipantButton);
    newParticipantForm.appendChild(formHeader);

    const rowDiv = document.createElement("div");
    rowDiv.classList.add("row");

    const columnDiv1 = document.createElement("div");
    columnDiv1.classList.add("col-12", "col-sm-6", "mb-2");

    const formLabel1 = document.createElement("label");
    formLabel1.classList.add("form-label", "fs-4");
    formLabel1.setAttribute("for", `firstname${newParticipantId}`);
    formLabel1.innerHTML = "First Name";

    columnDiv1.appendChild(formLabel1);

    const formInput1 = document.createElement("input");
    formInput1.setAttribute("type", "text");
    formInput1.classList.add("form-control", "fs-5");
    formInput1.setAttribute("id", `firstname${newParticipantId}`);
    formInput1.setAttribute("name", `firstname${newParticipantId}`);
    formInput1.required = true;

    columnDiv1.appendChild(formInput1);

    rowDiv.appendChild(columnDiv1);

    const columnDiv2 = document.createElement("div");
    columnDiv2.classList.add("col-12", "col-sm-6", "mb-2");

    const formLabel2 = document.createElement("label");
    formLabel2.classList.add("form-label", "fs-4");
    formLabel2.setAttribute("for", `lastname${newParticipantId}`);
    formLabel2.innerHTML = "Last Name";

    columnDiv2.appendChild(formLabel2);

    const formInput2 = document.createElement("input");
    formInput2.setAttribute("type", "text");
    formInput2.classList.add("form-control", "fs-5");
    formInput2.setAttribute("id", `lastname${newParticipantId}`);
    formInput2.setAttribute("name", `lastname${newParticipantId}`);
    formInput2.required = true;

    columnDiv2.appendChild(formInput2);
    rowDiv.appendChild(columnDiv2);
    newParticipantForm.appendChild(rowDiv);

    const formLabel3 = document.createElement("label");
    formLabel3.classList.add("form-label", "fs-4");
    formLabel3.setAttribute("for", `Email${newParticipantId}`);
    formLabel3.innerHTML = "Email";

    newParticipantForm.appendChild(formLabel3);

    const formInput3 = document.createElement("input");
    formInput3.setAttribute("type", "email");
    formInput3.classList.add("form-control", "fs-5");
    formInput3.setAttribute("id", `email${newParticipantId}`);
    formInput3.setAttribute("name", `email${newParticipantId}`);
    formInput3.required = true;

    newParticipantForm.appendChild(formInput3);
    participantsContainer.appendChild(newParticipantForm);

    hideOrShowDeleteButtons();
}

function deleteParticipant(participantId) {
    const participantsList = document.getElementById("participants").children;

    for (let i = 0; i < participantsList.length; i++) {
        if (participantsList[i].id == participantId) {
            for (let j = i; j < participantsList.length-1; j++) {
                document.getElementById(`firstname${j+1}`).value = document.getElementById(`firstname${j+2}`).value;
                document.getElementById(`lastname${j+1}`).value = document.getElementById(`lastname${j+2}`).value;
                document.getElementById(`email${j+1}`).value = document.getElementById(`email${j+2}`).value;
            }

            participantsList[participantsList.length-1].remove();
            break;
        }
    }

    hideOrShowDeleteButtons();
}

function hideOrShowDeleteButtons() {
    const participantCount = document.getElementById("participants").children.length;
    const deleteButtons = document.getElementsByClassName("delete");
    
    for (let i = 0; i < deleteButtons.length; i++) {
        if (participantCount <= 3) {
            deleteButtons[i].classList.add("invisible");
        } else {
            deleteButtons[i].classList.remove("invisible");
        }
    }
}
