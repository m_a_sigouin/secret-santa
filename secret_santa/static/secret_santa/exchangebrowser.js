let allTab;
let createdTab;
let attendingTab;
let invitedTab;

document.addEventListener("DOMContentLoaded", () => {
    allTab = document.getElementById("all");
    createdTab = document.getElementById("created");
    attendingTab = document.getElementById("attending");
    invitedTab = document.getElementById("invited");

    document.getElementById("all-btn").onclick = () => {
        changeTab("all");
    };

    document.getElementById("created-btn").onclick = () => {
        changeTab("created");
    };

    document.getElementById("attending-btn").onclick = () => {
        changeTab("attending");
    };

    document.getElementById("invited-btn").onclick = () => {
        changeTab("invited");
    };
});

function changeTab(tabName) {
    const allBtn = document.getElementById("all-btn");
    const createdBtn = document.getElementById("created-btn");
    const attendingBtn = document.getElementById("attending-btn");
    const invitedBtn = document.getElementById("invited-btn");

    switch (tabName) {
        case "all":
            allBtn.classList.add("active");
            allTab.classList.remove("d-none");
            createdBtn.classList.remove("active");
            createdTab.classList.add("d-none");
            attendingBtn.classList.remove("active");
            attendingTab.classList.add("d-none");
            invitedBtn.classList.remove("active");
            invitedTab.classList.add("d-none");
            break;

        case "created":
            allBtn.classList.remove("active");
            allTab.classList.add("d-none");
            createdBtn.classList.add("active");
            createdTab.classList.remove("d-none");
            attendingBtn.classList.remove("active");
            attendingTab.classList.add("d-none");
            invitedBtn.classList.remove("active");
            invitedTab.classList.add("d-none");
            break;
    
        case "attending":
            allBtn.classList.remove("active");
            allTab.classList.add("d-none");
            createdBtn.classList.remove("active");
            createdTab.classList.add("d-none");
            attendingBtn.classList.add("active");
            attendingTab.classList.remove("d-none");
            invitedBtn.classList.remove("active");
            invitedTab.classList.add("d-none");
            break;

        case "invited":
            allBtn.classList.remove("active");
            allTab.classList.add("d-none");
            createdBtn.classList.remove("active");
            createdTab.classList.add("d-none");
            attendingBtn.classList.remove("active");
            attendingTab.classList.add("d-none");
            invitedBtn.classList.add("active");
            invitedTab.classList.remove("d-none");
            break;
    
        default:
            break;
    }
}
