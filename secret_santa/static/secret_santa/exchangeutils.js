function replyToInvitation(exchangeId, participantId, answer) {
    fetch(`/reply/${exchangeId}`, {
        method: "PUT",
        body: JSON.stringify({
            participant_id: participantId,
            answer: answer
        })
    })
    .then(response => {
        console.log(response.status);

        if (response.status === 204) {
            const message = (answer === "Y" ? "Confirmation " : answer === "N" ? "Refusal " : "Other invalid choice ") + "sent.";

            alert(message);

            location.reload(true);
        } else {
            const data = response.json();
            console.log(data["error"]);
        }
    });
}

function getPickedName(exchangeId, participantId) {
    fetch(`/exchange/${exchangeId}/${participantId}`)
    .then(response => response.json())
    .then(result => {
        if (!result["name"] && result["error"]) {
            console.log(result["error"]);
        } else {
            document.getElementById("pickedNameLabel").innerHTML = result["name"];
        }
    });
}
