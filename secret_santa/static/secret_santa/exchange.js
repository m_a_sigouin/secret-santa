document.addEventListener("DOMContentLoaded", () => {
    const editForm = document.getElementById("edit-form");

    if (editForm) {
        editForm.onsubmit = editExchange;
    }
    
    const addParticipantForm = document.getElementById("add-participant-form");

    if (addParticipantForm) {
        addParticipantForm.onsubmit = addParticipant;
    }
});

function editExchange() {
    const exchangeId = document.getElementById("edit-exchange-id").value;
    const groupName = document.getElementById("name").value;
    const description = document.getElementById("description").value;
    const budget = document.getElementById("budget").value;
    const date = document.getElementById("date").value;

    fetch(`/exchange/${exchangeId}/edit`, {
        method: "POST",
        body: JSON.stringify({
            group_name: groupName,
            description: description,
            budget: budget,
            date: date
        }),
        headers: {"X-CSRFToken": document.getElementsByName("csrfmiddlewaretoken")[0].value},
        credentials: "same-origin"
    })
    .then(response => response.json())
    .then(result => {
        if (!result["error"] && result["message"]) {
            location.reload(true);
        } else {
            console.log(result["error"]);
        }
    });

    return false;
}

function cancelExchange(exchangeId) {
    const cancelBtn = document.getElementById("cancel-btn")

    const toCancel = cancelBtn.dataset.iscancelled !== "true";

    fetch(`/exchange/${exchangeId}/cancel`, {
        method: "PUT",
        body: JSON.stringify({
            cancel: toCancel
        })
    })
    .then(response => response.json())
    .then(result => {
        if (!result["error"] && result["message"]) {
            location.reload(true);
        } else {
            console.log(result["error"]);
        }
    });
}

function addParticipant() {
    const exchangeId = document.getElementById("add-exchange-id").value;
    const firstName = document.getElementById("firstname").value;
    const lastName = document.getElementById("lastname").value;
    const email = document.getElementById("email").value;

    fetch(`/exchange/${exchangeId}/add`, {
        method: "POST",
        body: JSON.stringify({
            firstname: firstName,
            lastname: lastName,
            email: email
        }),
        headers: {"X-CSRFToken": document.getElementsByName("csrfmiddlewaretoken")[0].value},
        credentials: "same-origin"
    })
    .then(response => response.json())
    .then(result => {
        if (!result["error"] && result["message"]) {
            location.reload(true);
        } else {
            console.log(result["error"]);
            alert(result["error"]);
        }
    });

    return false;
}

function makeDraw(exchangeId) {
    fetch(`/draw-names/${exchangeId}`, {
        method: "POST"
    })
    .then(response => response.json())
    .then(result => {
        if (!result["error"] && result["message"]) {
            location.reload(true);
        } else {
            console.log(result["error"]);
        }
    });
}

function resetDraw(exchangeId) {
    fetch(`/reset-draw/${exchangeId}`, {
        method: "PUT"
    })
    .then(response => response.json())
    .then(result => {
        if (!result["error"] && result["message"]) {
            location.reload(true);
        } else {
            console.log(result["error"]);
        }
    });
}
