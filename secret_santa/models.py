from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.

class Participant(models.Model):
    INVITED = "I"
    DECLINED = "N"
    CONFIRMED = "Y"
    NOT_ANSWERED = "U"

    PARTICIPATION_CHOICES = [
        (INVITED, "Invited"),
        (DECLINED, "Declined"),
        (CONFIRMED, "Confirmed"),
        (NOT_ANSWERED, "Not Answered")
    ]

    first_name = models.CharField(max_length=100, blank=False)
    last_name = models.CharField(max_length=100, blank=False)
    email = models.EmailField(blank=False)
    linked_user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    participation = models.CharField(choices=PARTICIPATION_CHOICES, default=INVITED, max_length=1)
    picked_participant = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)

    def check_if_participation_confirmed(self):
        return self.participation == self.CONFIRMED

    def check_if_participation_unanswered(self):
        return self.participation == self.INVITED
    
    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"


class Exchange(models.Model):
    group_name = models.CharField(max_length=100, blank=False)
    description = models.CharField(max_length=500, blank=True)
    budget = models.IntegerField(default=1, validators=[
        MinValueValidator(1)
    ])
    created = models.DateField(null=True)
    exchange_date = models.DateField(null=True)
    organizer = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, related_name="events")
    participants = models.ManyToManyField(Participant, blank=True, related_name="exchange")
    draw_made = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)


    def get_confirmed_participants(self):
        return self.participants.filter(participation=Participant.CONFIRMED)
    

    def get_not_participating(self):
        return self.participants.exclude(participation=Participant.CONFIRMED)


    def get_unanswered_participation_count(self):
        return self.participants.filter(participation=Participant.INVITED).count()


    def get_confirmed_participation_count(self):
        return self.participants.filter(participation=Participant.CONFIRMED).count()


    def get_declined_participation_count(self):
        return self.participants.filter(participation=Participant.DECLINED).count()
    
    
    def get_not_answered_participation_count(self):
        return self.participants.filter(participation=Participant.NOT_ANSWERED).count()

