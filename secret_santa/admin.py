from django.contrib import admin
from .models import Exchange, Participant

# Register your models here.
admin.site.register(Exchange)
admin.site.register(Participant)
