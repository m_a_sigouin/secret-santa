from django.urls import path

from . import views

#app_name = "secret_santa"
urlpatterns = [
    path("", views.index, name="index"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("register", views.register, name="register"),
    path("create-exchange", views.create_exchange, name="create"),
    path("my-exchanges", views.view_user_exchanges, name="exchanges"),
    path("exchange/<int:exchange_id>", views.view_exchange, name="exchange"),
    
    # API
    path("exchange/<int:exchange_id>/edit", views.edit_exchange, name="edit"),
    path("exchange/<int:exchange_id>/cancel", views.cancel_exchange, name="cancel"),
    path("exchange/<int:exchange_id>/add", views.add_participant_to_exchange, name="add"),
    path("exchange/<int:exchange_id>/<int:participant_id>", views.get_picked_name, name="name"),
    path("reply/<int:exchange_id>", views.invite_reply, name="reply"),
    path("draw-names/<int:exchange_id>", views.draw_names, name="draw"),
    path("reset-draw/<int:exchange_id>", views.reset_draw, name="reset"),
]
